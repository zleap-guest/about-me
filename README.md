# AboutMe

I wrote this as the 'About Me' application in Debian appears to have a problem.  So I wrote this as possible replacement.

![About Me Image](AboutMe.png)

 - **Language :** Python
 - **Version :** alpha
 - **Created :** 8/2/2019
 - **Code License :** GPL 2/3 is fine
 - **Screen shot license :** Creative commons

**To do :** 

 - Need to add code to save data to 'data.txt' (need help)
 - Clean up code (need help)
 - Ensure it is written in good Python3 (need help)
 - Add more shell choices if needed
 - Add more fields as appropriate

**Notes :** 

 - Source for pull down code credited
 - Should original app, change the user shell or just store data (unknown)
 - Added e-mail and mobile number as additional fields

**Requirements**
 - Python
 - PythonTk
 - os
 - sys


**Reference and Context**

As stated earlier the About Me App in Debian seems to have a bug :-

![About Me Bug](aboutmeerror.png)


<!--stackedit_data:
eyJoaXN0b3J5IjpbLTM1NTE2OTc2NF19
-->