#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  input-box.py
#  
#  Copyright 2014 Paul Sutton <psutton@ER1401>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  https://stackoverflow.com/questions/14824163/how-to-get-the-input-from-the-tkinter-text-box-widget
import tkinter # note use of caps
from tkinter import *
import sys
import os

#set up
window = Tk()
window.title('AboutMe')
window.geometry("350x280") #width x height
window.resizable(0,0)

#pull down menu code - https://pythonspot.com/tk-dropdown-example/
tkvar = StringVar(window)

choices = { '/bin/bash','/bin/sh','/bin/dash','/bin/rbash','/bin/screen',"bin/tmux"}
tkvar.set('/bin/bash') # set the default option


#read data from file
#content = open("data.txt", "r") as content:
#    content = [x.strip()for x in content]
#print (x)
#print ("Name of the file: ", fo.name)
#print ("Closed or not : ", fo.closed)
#print ("Opening mode : ", fo.mode)
#content.close()


#content = f.readlines()
# you may also want to remove whitespace characters like `\n` at the end of each line
#content = [x.strip() for x in content] 
        
#save field data to a "data.txt"
def apply():
    text = namebx.get("1.0",END)
    text2 = officebx.get("1.0",END)
    text3 = ophonebx.get("1.0",END)
    text4 = hphonebx.get("1.0",END)
    text5 = mphonebx.get("1.0",END)
    text6 = emailbx.get("1.0",END)
   #print(text)
    with open("data.txt", "w") as f:
       f.write(text)
       f.write(text2)
       f.write(text3)
       f.write(text4)
       f.write(text5)
       f.write(text6)
   
#this should take data from each of the boxes, and save to file data.txt

def exit():
    print("close")
    sys.exit()

#define boxes
namebx = Text(window, height=1, width=20)
officebx = Text(window, height=1, width=20)
ophonebx  = Text(window, height=1, width=20)
hphonebx = Text(window, height=1, width=20)
mphonebx = Text(window, height=1, width=20)
emailbx = Text(window, height=1, width=20)

#define labels
name = Label(window, text="Name: ")
office = Label(window, text="Office: ")
ophone = Label(window, text="Office Phone:")
hphone = Label(window, text="Home Phone: ")
mphone = Label(window, text="Mobile Phone:")
email = Label(window, text="E-mail")


#display boxes

namebx.grid(row = 1, column = 4,)
officebx.grid(row = 2, column = 4,)
ophonebx.grid(row = 3, column = 4,)
hphonebx.grid(row = 4, column = 4,)
mphonebx.grid(row = 5, column = 4,)
emailbx.grid(row = 6, column = 4,)

#place labels

name.grid(row = 1, column = 1, padx = 5, pady = 5)
office.grid(row = 2, column = 1, padx = 5, pady = 5)
ophone.grid(row = 3, column = 1, padx = 5, pady = 5)
hphone.grid(row = 4, column = 1, padx = 5, pady = 5)
mphone.grid(row = 5, column = 1, padx = 5, pady = 5)
email.grid(row = 6, column = 1, padx = 5, pady = 5)
#shel.grid(row = 9, column = 1, padx = 5, pady = 5)

#define buttons
close = Button( window, text ='Close', command=exit )
apply= Button( window, text ='Apply', command=apply )

#place buttons
close.grid(row = 10, column = 1, padx = 5, pady = 5)
apply.grid(row = 10, column = 3, padx = 5, pady = 5)

#pull down menu
popupMenu = OptionMenu(window, tkvar, *choices)
Label(window, text="Shell: ").grid(row = 7, column = 1)
popupMenu.grid(row = 7, column = 4)


#display window

window.mainloop()
